def update_yarn_v
  text = File.read('package.json')
  new_contents = text.gsub('"yarn": "1.6.0"', '"yarn": "^1.7.0"')
  File.open('package.json', "w") {|file| file.puts new_contents }
end

def update_node_v
  text = File.read('package.json')
  new_contents = text.gsub('"node": "8.10.0"', '"node": "^8.11.3"')
  File.open('package.json', "w") {|file| file.puts new_contents }
end

def replace_upd_deps
  text = File.read('package.json')
  new_contents = text.gsub('"build": "yarn run upd_deps && gulp build"', '"build": "gulp build"')
  File.open('package.json', "w") {|file| file.puts new_contents }
end


branches = [
  'autoeurope',
  'economybookings',
  'blablacar',
  'blablacar_extended_trips',
  'busfor',
  'tutu',
  'tutu_extended_trips',
  'unitiki'
]

branches.each_with_index do |b, index|
  print "Switching to branch \"#{ index + 1 }. #{b}\"...\n"
  `git checkout #{b}`

  print "Updating yarn...\n"
  update_yarn_v

  print "Updating node...\n"
  update_node_v

  print "Replace deps script...\n"
  replace_upd_deps

  print "Upgrading deps...\n"
  `yarn upgrade --check-files`

  print "Build & deploy...\n"
  `yarn run deploy`
  sleep 5

  print "Try commit changes...\n"
  str = `git commit -am 'upd deps'`
  unless str =~ /nothing added to commit/
    print "Push changes...\n"
    `git push`
  end
  print "#{b} - done.\n\n"
end
