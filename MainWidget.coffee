jsonp = require('jsonp')
AutocompleteClient = require('./AutocompleteClient')
mamka = require('mamka')


# NOTE: POLYFILL FOR "node.closest()"
`
if (!Element.prototype.matches)
  Element.prototype.matches = Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector;

if (!Element.prototype.closest)
  Element.prototype.closest = function(s) {
    var el = this;
    if (!document.documentElement.contains(el)) return null;
    do {
      if (el.matches(s)) return el;
      el = el.parentElement || el.parentNode;
    } while (el !== null && el.nodeType === 1);
    return null;
  };
`

MAMKA_EVENTS =
  INIT: 'init'
  LEAD: 'lead'
  INTERACT: 'interact'

IS_PRODUCTION = process.env.NODE_ENV is 'production'

class Mamka
  constructor: ({ props, params, scriptSrc }) ->
    @mamka = mamka
    @mamka('create', props)
    @params = params
    @scriptSrc = @formatScriptSrc(scriptSrc)

    @sendPerf()

  formatScriptSrc: (scriptSrc) =>
    unless scriptSrc?
      return scriptSrc

    unless scriptSrc.startsWith('http')
      scriptSrc = window.location.origin + (if scriptSrc.startsWith('/') then '' else '/') + encodeURI(scriptSrc)

    return scriptSrc

  sendPerf: =>
    unless @scriptSrc?
      return

    setTimeout(
      =>
        perfEvents = performance.getEntriesByName(@scriptSrc)
        perfEvents = perfEvents.map((x) -> x.toJSON())
        for x in perfEvents
          @send('perf', x)
      5000
    )

  send: (name, meta) =>
    # NOTE: IOS 8 SAFARI COMPATIBILIYY
    unless meta?
      meta = {}

    params = Object.assign({}, meta, @params)
    @mamka('send_event', { name, meta: params })


MainWidget = (props) ->
  @_jsonpCallbackPrefix = null
  @_autocompleteClient = null
  @_mamka = null
  @_isInteracted = false

  {
    autocompleteClientProps
    mamkaProps
    widgetId
  } = props

  @mamkaProps = mamkaProps
  @state._MAMKA_EVENTS = MAMKA_EVENTS

  unless @state.widgetId?
    throw new Error('No widgetId in MainWidget')

  @_jsonpCallbackPrefix = @state.widgetId.replace(/\-/g, '_') + '_'

  @jsonp = ({ url, callbackName, jsonpProps, callback }) =>
    unless url? and callbackName? and callback?
      throw new Error('Some params are missing for jsonp')

    jsonpProps ||= {}
    jsonpProps.prefix = @_jsonpCallbackPrefix + '_' + callbackName + '_'
    jsonp(
      url
      jsonpProps
      callback
    )

  @_sendMetric = (name) =>
    unless @mamkaProps?
      return

    if name is @state._MAMKA_EVENTS.INTERACT
      if @_isInteracted
        return
      @_isInteracted = true

    unless IS_PRODUCTION
      console.log "SEND METRIC -> #{name}"
      return

    switch name
      when @state._MAMKA_EVENTS.INIT
        @_mamka.send(@state._MAMKA_EVENTS.INIT)
      else
        @_mamka.send(name, {})

  @_initMetrics = =>
    @_mamka = new Mamka({
      props: @mamkaProps.props
      params: @mamkaProps.params
      scriptSrc: @mamkaProps.scriptSrc
    })

    @_sendMetric(@state._MAMKA_EVENTS.INIT)
    document.querySelector("##{@state.widgetId}")
      .addEventListener 'click', (e) =>
        nodeWithMetric = e.target.closest('[data-uxiemetric]')
        unless nodeWithMetric?
          return

        { uxiemetric } = nodeWithMetric.dataset
        unless uxiemetric?
          return

        @_sendMetric( uxiemetric )

  if autocompleteClientProps?
    autocompleteClientProps.jsonp = @jsonp
    @_autocompleteClient = new AutocompleteClient(
      autocompleteClientProps
    )

getWidgetId = (prefix) ->
  return "#{prefix}-#{(new Date).getTime()}"

module.exports = {
  MainWidget,
  getWidgetId,
  Mamka,
}
