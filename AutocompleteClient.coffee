# NOTE: EXPECTS
# jsonp: func to call jsonp lib
# url: url to call
# callbackName: name of callback func for jsonp
# urlProps: props for url
# processData: func to process data before giving back

class AutocompleteClient
  constructor: (props) ->
    {
      jsonp
      url
      callbackName
      urlProps
      processData
      termParam
      processTerm
      jsonpProps
    } = props

    @jsonp = jsonp
    @url = url
    @callbackName = callbackName
    @urlProps = urlProps or {}
    @urlProps.limit ||= '10'
    @processData = processData
    @termParam = termParam or 'query'
    @jsonpProps = jsonpProps
    @processTerm = processTerm

  get: ({ term, callback, callbackName, urlProps }) =>
    url = @url.slice()

    url += if @processTerm?
      @processTerm(term)
    else
      "?#{@termParam}=#{encodeURIComponent(term)}"

    urlProps = Object.assign({}, @urlProps, urlProps or {})

    for k,v of urlProps
      url += "&#{k}=#{v}"

    @jsonp({
      url
      jsonpProps: @jsonpProps
      callbackName: callbackName or @callbackName
      callback: (error, data) =>
        if data?
          if @processData?
            data = @processData(data)
          callback({ data })
        else
          console.log ("autocomplete 500: #{error}")
    })

module.exports = AutocompleteClient
